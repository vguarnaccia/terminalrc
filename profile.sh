# shellcheck shell=sh
REPO="${HOME}/.dotfiles/"

# shellcheck source=/dev/null
test -e "${REPO}/local.sh" && . "${REPO}/local.sh"

check_cmd() {
	command -v "$1" >/dev/null 2>&1
	return $?
}

##############################################################################
# Manage PATH
##############################################################################
prependpath() {
	case ":$PATH:" in
	*:"$1":*) ;;

	*)
		PATH="$1${PATH:+:${PATH}:}"
		;;
	esac
}

prependpath "${HOME}/.local/bin"   # XDG binaries compatibility.
prependpath "${HOME}/.cargo/bin"   # Rust binaries compatibility.
check_cmd go && prependpath "$(go env GOPATH)/bin" # GO binaries compatibility.

###############################################################################
# Colorize
###############################################################################

alias less='less -XF --RAW-CONTROL-CHARS --LINE-NUMBERS --LONG-PROMPT'

# colored GCC warnings and errors
export GCC_COLORS='error=01;31:warning=01;35:note=01;36:caret=01;32:locus=01:quote=01'

# Default to human readable figures
alias df='df -h'
alias du='du -h'

# At least colorize man pages
man() {
	env \
		LESS_TERMCAP_mb="$(printf "\e[1;31m")" \
		LESS_TERMCAP_md="$(printf "\e[1;31m")" \
		LESS_TERMCAP_me="$(printf "\e[0m")" \
		LESS_TERMCAP_se="$(printf "\e[0m")" \
		LESS_TERMCAP_so="$(printf "\e[1;44;33m")" \
		LESS_TERMCAP_ue="$(printf "\e[0m")" \
		LESS_TERMCAP_us="$(printf "\e[1;32m")" \
		man "$@"
}

###############################################################################
# Safety first
###############################################################################
# Safer `rm`
if check_cmd trash; then
	alias rm='trash'
else
	alias rm='rm -I'
fi

alias cp='cp -i'
alias mv='mv -i'

###############################################################################
# Misc
###############################################################################
alias gl='git log --graph --abbrev-commit --decorate --date=relative --format=format:'\''%C(bold blue)%h%C(reset) %G? %C(bold green)(%ar)%C(reset) %C(white)%s%C(reset) %C(dim white)- %an%C(reset)%C(bold yellow)%d%C(reset)'\'' --all'
alias whence='type -a' # a better `which`

# Local weather
wttr() {
	# change pasadena-ca to your default location
	local request="wttr.in/${1-pasadena-ca}"
	[ "$COLUMNS" -lt 125 ] && request+='?n'
	curl -H "Accept-Language: ${LANG%_*}" --compressed "$request"
}

# Set text editor.
if check_cmd nvim; then
	export VISUAL="nvim"
else
	export VISUAL="vim"
fi
export EDITOR="${VISUAL}"
alias vi="${VISUAL}"
alias vim="${VISUAL}"

# Alias an improved `ls`
alias l="exa --classify --git-ignore"
alias la="exa --classify --all"
alias ll="exa --git --git-ignore --classify --header --long --group"
alias lla="exa --git --classify --header --long --group --all"
alias lt="exa  --git --git-ignore --classify --tree"
alias lta="exa  --git --classify --tree --all"
alias llt="exa -abghHliST --classify --git"
alias llta=llt
