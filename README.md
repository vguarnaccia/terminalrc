## dotfiles

This is how I setup my dot files. Vimrcs are not currently included.

There are [other, more useful dotfiles repos][1] you may wish to take a look at.

### Prerequisites

All you need is POSIX shell such as bash or zsh. Git is optional but strongly recommended for cloning.

### Installation

Install on bash with:

```dash
$ git clone https://gitlab.com/vguarnaccia/dotfiles.git ~/.dotfiles
$ ~/.dotfiles/install.sh
```

Uninstall, `rm ~/.dotfiles`. Then remove broken symlinks in `$HOME`. 

### Extendible

If you need any local configurations, such as environment variables, you can add them to `$HOME/.dotfiles/local.sh`.

Shell specific local configurations go to `local.bash` or `local.zsh`.

### Unwarranted Suggestions

1. Replace `ls` with [exa][exa].
2. Replace `grep` with [rg][ripgrep].
3. Replace `find` with [fd][fd].
4. Replace `less` and `cat` with [bat][bat].
5. `locate` is superior to `find` if the file you are looking for is a least a day old.

[1]: https://wiki.archlinux.org/index.php/Dotfiles
[exa]: https://the.exa.website/
[ripgrep]: https://github.com/BurntSushi/ripgrep
[fd]: https://github.com/sharkdp/fd
[bat]: https://github.com/sharkdp/bat
