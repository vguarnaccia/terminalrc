#! /bin/sh -x

REPO="${HOME}/.dotfiles/"
ln -s "${REPO}/bashrc.bash" "${HOME}/.bashrc"
ln -s "${REPO}/zshrc.zsh" "${HOME}/.zshrc"
